package controller;

import java.io.IOException;
import java.util.Scanner;

import model.data_structures.LinkedList;
import model.data_structures.Node;
import model.data_structures.SequentialLinkedList;
import model.logic.MVCModelo;
import model.logic.Uber;
import view.MVCView;

public class Controller {

	/* Instancia del Modelo*/
	private MVCModelo modelo;
	
	/* Instancia de la Vista*/
	private MVCView view;
	
	/**
	 * Crear la vista y el modelo del proyecto
	 * @param capacidad tamaNo inicial del arreglo
	 */
	public Controller ()
	{
		view = new MVCView();
		modelo = new MVCModelo();
	}
		
	public void run() 
	{
		Scanner lector = new Scanner(System.in);
		boolean fin = false;
		String dato = "";
		String respuesta = "";

		while( !fin ){
			view.printMenu();

			int option = lector.nextInt();
			switch(option){
			
				case 1:					
					try {
						modelo.cargar();
						LinkedList lista = modelo.lista();
						System.out.println("El n�mero de viajes cargado:" + lista.size());
						Node<Uber> Primero = modelo.lista().getFirst();
						System.out.println("Zona de origen:" + Primero.getElement().getSourceId());
						System.out.println("Zona de destino:" + Primero.getElement().getDstid());
						System.out.println("D�a:" + Primero.getElement().getDay());
						System.out.println("Tiempo promedio:" + Primero.getElement().getMeanTravel_time());
						
						Node<Uber> ultimo = modelo.lista().getLast();
						System.out.println("Zona de origen:" + ultimo.getElement().getSourceId());
						System.out.println("Zona de destino:" + ultimo.getElement().getDstid());
						System.out.println("D�a:" + ultimo.getElement().getDay());
						System.out.println("Tiempo promedio:" + ultimo.getElement().getMeanTravel_time());
						
					} catch (Exception e1) {
						e1.printStackTrace();
					}
					break;
			
				case 2:
					break;
				
				case 3:
					fin = true;
					break;
				
				default: 
					System.out.println("--------- \n Opcion Invalida !! \n---------");
					break;
			}
		}
	} 
	
}
