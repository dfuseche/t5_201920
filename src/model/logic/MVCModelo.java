package model.logic;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Random;
import java.util.stream.IntStream;

import model.data_structures.LinearProbingHashTable;
import model.data_structures.LinkedList;
import model.data_structures.Node;
import model.data_structures.SeparateChainingHashTable;
import model.data_structures.SequentialLinkedList;



/**
 * Definicion del modelo del mundo
 *
 */
public class MVCModelo {
	/**
	 * Atributos del modelo del mundo
	 */


	private LinkedList lista;
	private SequentialLinkedList seqLista;

	private int tamanio;

	public MVCModelo()
	{
		lista = new LinkedList ();
		seqLista = new SequentialLinkedList ();
	}

	/**
	 * Servicio de consulta de numero de elementos presentes en el modelo 
	 * @return numero de elementos presentes en el modelo
	 */
	public int darTamano()
	{
		return lista.size();
	}

	public int darTamano1 ()
	{
		return seqLista.getSize(); 
	}



	public void cargar () throws Exception
	{

		String ruta = "\\data\\bogota-cadastral-2018-1-WeeklyAggregate.csv";
		String ruta2 = "\\data\\bogota-cadastral-2018-2-WeeklyAggregate.csv";
		String ruta3 = "\\data\\bogota-cadastral-2018-3-WeeklyAggregate.csv";
		String ruta4 = "\\data\\bogota-cadastral-2018-4-WeeklyAggregate.csv";
		BufferedReader br = new BufferedReader (new FileReader (ruta));
		String [] datos = new String[7];
		String Linea = br.readLine();
		Linea = br.readLine();

		BufferedReader br2 = new BufferedReader (new FileReader (ruta2));
		String [] datos2 = new String[7];
		String Linea2 = br2.readLine();
		Linea2 = br2.readLine();
		
		BufferedReader br3 = new BufferedReader (new FileReader (ruta3));
		String [] datos3 = new String[7];
		String Linea3 = br3.readLine();
		Linea3 = br3.readLine();
		
		BufferedReader br4 = new BufferedReader (new FileReader (ruta4));
		String [] datos4 = new String[7];
		String Linea4 = br4.readLine();
		Linea4 = br4.readLine();

		while (Linea != null){

			datos = Linea.split(",");
			double sId = Double.parseDouble(datos[0].trim());
			double dstId = Double.parseDouble(datos[1].trim());
			double hour = Double.parseDouble(datos[2].trim());
			double mtT = Double.parseDouble(datos[3].trim());
			double sdtT = Double.parseDouble(datos[4].trim());
			double gmtT = Double.parseDouble(datos[5].trim());
			double gsdtT = Double.parseDouble(datos[6].trim());

			Uber nuevo = new Uber (sId, dstId, hour, mtT, sdtT, gmtT, gsdtT);
			lista.addAtPos(darTamano(), nuevo);
			seqLista.put(sId, nuevo);

			Linea = br.readLine();
		}

		br.close();

		while(Linea2 != null)
		{

			datos = Linea.split(",");
			double sId = Double.parseDouble(datos2[0].trim());
			double dstId = Double.parseDouble(datos2[1].trim());
			double hour = Double.parseDouble(datos2[2].trim());
			double mtT = Double.parseDouble(datos2[3].trim());
			double sdtT = Double.parseDouble(datos2[4].trim());
			double gmtT = Double.parseDouble(datos2[5].trim());
			double gsdtT = Double.parseDouble(datos2[6].trim());

			Uber nuevo = new Uber (sId, dstId, hour, mtT, sdtT, gmtT, gsdtT);
			lista.addAtPos(darTamano(), nuevo);
			seqLista.put(sId, nuevo);

			Linea2 = br2.readLine();
		}

		br2.close();
		
		while(Linea3 != null)
		{

			datos = Linea.split(",");
			double sId = Double.parseDouble(datos3[0].trim());
			double dstId = Double.parseDouble(datos3[1].trim());
			double hour = Double.parseDouble(datos3[2].trim());
			double mtT = Double.parseDouble(datos3[3].trim());
			double sdtT = Double.parseDouble(datos3[4].trim());
			double gmtT = Double.parseDouble(datos3[5].trim());
			double gsdtT = Double.parseDouble(datos3[6].trim());

			Uber nuevo = new Uber (sId, dstId, hour, mtT, sdtT, gmtT, gsdtT);
			lista.addAtPos(darTamano(), nuevo);
			seqLista.put(sId, nuevo);

			Linea3 = br3.readLine();
		}

		br3.close();
		
		while(Linea4 != null)
		{

			datos = Linea.split(",");
			double sId = Double.parseDouble(datos4[0].trim());
			double dstId = Double.parseDouble(datos4[1].trim());
			double hour = Double.parseDouble(datos4[2].trim());
			double mtT = Double.parseDouble(datos4[3].trim());
			double sdtT = Double.parseDouble(datos4[4].trim());
			double gmtT = Double.parseDouble(datos4[5].trim());
			double gsdtT = Double.parseDouble(datos4[6].trim());

			Uber nuevo = new Uber (sId, dstId, hour, mtT, sdtT, gmtT, gsdtT);
			lista.addAtPos(darTamano(), nuevo);
			seqLista.put(sId, nuevo);

			Linea4 = br4.readLine();
		}

		br4.close();
	}
	
	public LinkedList lista ()
	{
		return lista; 
	}
	
	public SequentialLinkedList sLista ()
	{
		return seqLista; 
	}
}
