package model.data_structures;

import java.util.Iterator;

public class SeparateChainingHashTable <K,V>
{
	private SequentialLinkedList<K,V>[] hashTable;
	private int size;
	private int keyValuesStored;

	public SeparateChainingHashTable(int m)
	{
		size = m;
		hashTable = new SequentialLinkedList[m];
		for (int i = 0; i < size; i++)
		{
			hashTable[i] = new SequentialLinkedList<K,V>();
		}
	}

	public void reHash(int lists)
	{
		SeparateChainingHashTable<K, V> temp = new SeparateChainingHashTable<K, V>(lists);
		for ( int i = 0; i < size; i++)
		{
			for ( K key: hashTable[i].keys())
			{
				temp.put(key, hashTable[i].getValue(key));
			}
		}

		this.size = temp.size;
		this.keyValuesStored = temp.keyValuesStored;
		this.hashTable = temp.hashTable;
	}

	public int hash(K key)
	{
		int hash;
		hash = key.hashCode() & 0x7fffffff % size;
		return hash;
	}

	public void put(K pKey,V pValue)
	{
		if ( keyValuesStored / size > 0.6)
		{
			reHash(2*size);
		}

		int h = hash(pKey);
		if (!hashTable[h].containsKey(pKey))
		{
			keyValuesStored++;
		}
		try {
			hashTable[h].put(pKey, pValue);
		} 
		catch (Exception e) 
		{

			e.printStackTrace();
		}
	}
	public V get(K pKey)
	{
		V ans = null;
		int h = hash(pKey);
		ans = hashTable[h].getValue(pKey);

		return ans;
	}
	
	public boolean contains ( K key)
	{
		return get(key) != null;
	}
	public V delete(K pKey)
	{
		V ans = null;

        int i = hash(pKey);
        if (hashTable[i].containsKey(pKey)) 
        	{
        	 keyValuesStored--;
        	}
        ans = hashTable[i].getValue(pKey);
        hashTable[i].delete(pKey);
		return ans;
	}
	public Iterable<K> keys()
	{
		Queue<K> listKeys = new Queue<K>();
        for (int i = 0; i < size; i++) {
            for (K key : hashTable[i].keys())
                listKeys.enqueue(key);
        }
        return listKeys;

	}
	
	public SequentialLinkedList<K,V>[] getHashTable()
	{
		return hashTable;
	}
	
	public int getSize()
	{
		return size;
	}
}
