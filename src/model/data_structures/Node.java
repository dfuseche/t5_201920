package model.data_structures;



public class Node<E> {

	private E element;
	private Node<E> next;


	public Node(E elem) 
	{
		element = elem;
		next = null;
	}

	public E getElement()
	{
		return element;
	}

	public Node<E> getNext()
	{
		return next;
	}




	public void changeNext(Node<E> pNew)
	{
		next = pNew;
	}

}


