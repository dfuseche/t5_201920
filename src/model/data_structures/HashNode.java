package model.data_structures;

public class HashNode <K,V>
{
	private K key;
	private V value;
	private HashNode<K, V> next;

	public HashNode(K pKey, V pValue) 
	{
		key = pKey;
		value = pValue;
	}

	public K getKey()
	{
		return key;
	}

	public V getValue() 
	{
		return value;
	}
	
	public void changeValue(V val)
	{
		value = val;
	}
	
	public void changeNext(HashNode<K, V> node)
	{
		next = node;
	}
	public HashNode<K, V> getNext()
	{
		return next;
	}


}
