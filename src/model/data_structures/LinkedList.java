package model.data_structures;

import java.util.Iterator;




public class LinkedList<E>  {

	private Node<E> first;
	private Node<E> last;
	private int size;


	public Integer size()
	{ 
		Node<E> node = first;

		while( node != null)
		{
			size++;
			node = node.getNext();
		}
		return size;
	}
	
	public Node<E> getFirst()
	{
		return first;
	}

	public Node<E> getLast()
	{
		return last;
	}


	public void addAtPos(int pos, E elem) throws IndexOutOfBoundsException
	{
		if (pos > size)
		{
			throw new IndexOutOfBoundsException();
		}
		if (first == null)
		{
			first = new Node<E>(elem);
		}
		else
		{
			Node<E> temp = first;
			int pos1 = 0;
			while (temp != null)
			{
				if (pos1 == pos)
				{
					Node<E> newOne = new Node<E> (elem);
					newOne.changeNext(temp.getNext());
					temp.changeNext(newOne);
					size++;
				}
				else 
				{
					temp = temp.getNext();
					pos1++;
				}
			}
		}

	}

	public void addAtEnd(E elem)
	{
		if(first == null)
		{
			first = new Node<E>(elem);
		}
		else
		{
			Node<E> temp = first;
			while(temp != null)
			{
				if (temp.getNext() == null)
				{
					temp.changeNext(new Node<E> (elem));
					size++;

				}
				else
				{
					temp = temp.getNext();
				}
			}
		}
	}

	public void deleteAtPos(int pos)
	{
		if ( pos  >= 0 && pos <= size)
		{
			int pos1 = 0;
			Node<E> temp = first;
			while(temp != null)
			{
				if(pos1 + 1 == pos)
				{
					temp.changeNext(temp.getNext().getNext());
					size--;
				}
				pos1++;
				temp = temp.getNext();
			}
		}
	}

	public E getElementAtPos(int pos)
	{
		E elem = null;
		if (first == null)
		{
			return null;
		}

		Node<E> current = first;
		int pos1 = 0;
		while (current != null)
		{
			if (pos1 == pos)
			{
				elem = current.getElement();
			}
			current = current.getNext();
			pos1++;
		}

		return elem;
	}

	public void exchange(E fir, E sec)
	{
		Node<E> prevFir= null;
		Node<E> tempFir = first;
		Node<E> prevSec = null;
		Node<E> tempSec = first;

		while(tempFir != null && tempFir.getElement() != fir)
		{
			prevFir = tempFir;
			tempFir = tempFir.getNext();
		}

		while(tempSec != null && tempSec.getElement() != sec)
		{
			prevSec = tempSec;
			tempSec = tempSec.getNext();
		}

		if (prevFir != null)
		{
			prevFir.changeNext(tempSec);
		}
		else
		{
			first = tempSec;
		}

		if( prevSec != null)
		{
			prevSec.changeNext(tempFir);
		}
		else
		{
			first = tempFir;
		}

		Node<E> aux = tempFir.getNext();
		tempFir.changeNext(tempSec.getNext());
		tempSec.changeNext(aux);

	}
}

