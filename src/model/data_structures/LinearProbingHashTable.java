package model.data_structures;

import java.util.Iterator;

public class LinearProbingHashTable<K, V>  
{
	private int keyValuesStored;
	private int size;
	private K  []keys;
	private V [] values;

	public LinearProbingHashTable (int pSize)
	{
		size = pSize;
		keyValuesStored = 0;
		keys = (K[]) new Object [size];
		values = (V[]) new Object [size];
	}

	public boolean contains (K key)
	{
		boolean con = false;

		try {
			if (get(key) != null)
			{
				con = true;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return con;
	}

	public V get(K key) throws Exception
	{
		V val = null;
		if (key != null)
		{
			for (int i = 0; i < keys.length; i++)
			{
				if (keys[i].equals(key))
				{
					val = values[i];
				}
			}

			return val;
		}
		else 
		{
			throw new Exception("Key cant be null");
		}
	}

	public int hash(K key)
	{
		int hash = key.hashCode() & 0x7fffffff % size;
		return hash;
	}


	public void put (K key, V val) throws Exception
	{
		if ( val != null)
		{
			if (keyValuesStored / size > 0.75)
			{
				reHash(2*size);
			}
			int hash = hash(key);
			int i;
			boolean found = false;
			for ( i = hash; keys[i] != null; i = (i + 1) % size )
			{
				if (keys[i].equals(key))
				{
					values[i] = val;
					found = true;
				}
			}
			if (!found)
			{
				keys[i] = key;
				values[i] = val;
				keyValuesStored++;
			}
		}
		else 
		{
			throw new Exception("Value cant be null");
		}
	}

	public void reHash(int pSize) 
	{
		LinearProbingHashTable<K, V> temp = new LinearProbingHashTable<K, V>(pSize);
		for (int i = 0; i < size; i++) 
		{
			if (keys[i] != null) 
			{
				try 
				{
					temp.put(keys[i], values[i]);
				} catch (Exception e) 
				{
					e.printStackTrace();
				}
			}
		}
		keys = temp.keys;
		values = temp.values;
		size= temp.size;
	}

	public V delete(K key)
	{
		V val = null;
		if (!contains(key))
		{
			val = null;
		}
		else 
		{
			int hashPos = hash (key);
			while (keys[hashPos].equals(key) == false)
			{
				hashPos = (hashPos + 1) % size;
			}

			val = values[hashPos];
			values[hashPos] = null;
			keys[hashPos ] = null;
		}
		return val;
	}

	public Iterable<K> keys() 
	{
		Queue<K> listKeys = new Queue<K>();
		for (int i = 0; i < size; i++)
		{
			if (keys[i] != null) 
			{
				listKeys.enqueue(keys[i]);
			}
		}

		return listKeys;
	}
	
	public int getSize()
	{
		return size;
	}

	public K[] getKeys()
	{
		return keys;
	}
	public V[] getValues()
	{
		return values;
	}
}
