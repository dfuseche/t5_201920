package model.data_structures;



public class SequentialLinkedList <K,V>
{
	private int keyValuesStored;
	private HashNode<K,V> first;

	public SequentialLinkedList ()
	{
		keyValuesStored = 0;
		first = null;
	}

	public int getSize()
	{
		return keyValuesStored;
	}

	public boolean empty()
	{
		boolean isEmpty = false;
		if (keyValuesStored == 0)
		{
			isEmpty = true;
		}
		return isEmpty;
	}

	public V getValue (K key)
	{
		V value = null;
		if (first == null)
		{
			value = null;
		}
		else
		{
			HashNode<K, V> temp = first;
			while (temp != null)
			{
				if (temp.getKey().equals(key))
				{
					value = temp.getValue();
				}

				temp = temp.getNext();
			}
		}
		return value;
	}

	public void put (K key, V value) throws Exception
	{
		if ( value != null)
		{
			HashNode<K, V> nvo = new HashNode(key, value);
			if (first == null)
			{
				first = nvo;
			}
			else
			{
				boolean keyExists = false;
				HashNode<K, V> temp = first;
				while (temp != null && !keyExists)
				{
					if (temp.getKey().equals(key))
					{
						temp.changeValue(value);
						keyExists = true;
					}
					if (temp.getNext() == null  && !keyExists)
					{
						temp.changeNext(nvo);
						keyValuesStored++;
					}
					temp = temp.getNext();
				}
			}
		}
		else 
		{
			throw new Exception("value can't be null");
		}
	}

	public boolean containsKey( K key)
	{
		boolean contains = false;
		if (getValue(key) != null)
		{
			contains = false;
		}
		return contains;
	}
	public V delete(K key)
	{
		V value = null;
		HashNode<K, V> temp = first;
		if (temp.getKey().equals(key))
		{
			value = first.getValue();
			first = temp.getNext();
		}
		while (temp != null)
		{
			if (temp.getNext().getKey().equals(key))
			{
				temp.changeNext(temp.getNext().getNext());
				value = temp.getValue();
				keyValuesStored--;
			}
			temp = temp.getNext();
		}
		return value;
	}

	public Iterable<K> keys()
	{
		Queue<K> keys = new Queue<K>();
		HashNode<K, V> temp = first;
		while (temp != null)
		{
			keys.enqueue(temp.getKey());
		}
		return keys;
	}
}
