package model.data_structures;

import java.util.Iterator;

public class Queue <K> implements Iterable<K>
{
	private Node<K> tail;
	private Node<K> head;
	int size;
	public Node<K> getHead()
	{
		return head;
	}


	public void enqueue(K key) 
	{

		Node<K> pNew = new Node<K>(key);
		tail.changeNext(pNew);
		tail = pNew;
		size++;

	}


	public K dequeue() 
	{
		K ans = head.getElement();
		head = head.getNext();
		size--;
		return ans;

	}

	public int size()
	{
		return size;

	}


	@Override
	public Iterator<K> iterator() {
		return new ListIterator<K>(head);  
		
	}
	
	
	private class ListIterator<K> implements Iterator<K> {
		private Node<K> temp;

		public ListIterator(Node<K> head) 
		{
			temp = head;
		}

		public boolean hasNext()  
		{ 
			return temp != null;    
		}
		public void remove()      
		{ 
			throw new UnsupportedOperationException();  
		}

		public K next() 
		{
			
			K item = temp.getElement();
			temp = temp.getNext();
			return item;
		}
	}
}

